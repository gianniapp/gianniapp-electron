version.middler = package.version;
middlerName = package.name;
opensource[package.name] = opensourcelicenses.mit('Marco Benzoni', '2018-2019');
opensource['discord-rpc'] = opensourcelicenses.apache('Discord.js contributors', '2016-2019');
opensource['electron'] = opensourcelicenses.mit('GitHub Inc.', '2013-2018');
baseUrl = '';
var middler = {};

const fs = remote.require('fs-extra');
const AdmZip = remote.require('adm-zip');

ipcRenderer.on('main-ok', function(_e, data) {
  middlerInternal = data;
  
  middler = {
    callbackMgr: {
      save: function(callback) {
        var callbackId = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < 12; i++ ) {
          callbackId += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        if (middler.callbackMgr.callbacks[callbackId]) return middler.callbackMgr.save(callback);
        middler.callbackMgr.callbacks[callbackId] = callback;
        return callbackId;
      },
      callbacks: {}
    },
    features: {
      externalDarkControl: middlerInternal.dark !== null,
      packageManagement: true,
      syncRedirect: true
    },
    getFeatureInfo: function(name) {
      if (name == 'externalDarkControl') {
        return middlerInternal.dark;
      } else if (name == 'syncRedirect') {
        return 'gianniapp-electron://sync-setup';
      }
    },
    packages: {
      location: {
        folderToLoc: function(folderName) {
          if (folderName == middlerInternal.paths.packages) {
            return 'main';
          } else if (folderName == middlerInternal.paths.managedPackages) {
            return 'managed-packages';
          } else {
            return undefined;
          }
        },
        locToFolder: function(location) {
          if (location == 'main') {
            return middlerInternal.paths.packages;
          } else if (location == 'managed') {
            return middlerInternal.paths.managedPackages;
          } else {
            return undefined;
          }
        }
      },
      getResourceLocation: function(location, id, type, resourceName) {
        if (location == 'middler') {
          throw 'there are no packages in middler';
        } else if (location == 'main' || location == 'managed') {
          var folderName = middler.packages.location.locToFolder(location);
          if (type.startsWith('fruit') && type.length == 'fruit'.length + 1) {
            var eatAmount = parseInt(type.substr('fruit'.length, 1));
            if (eatAmount >= 0 && eatAmount <= 3) {
              return fileUrl(path.join(folderName, id, 'fruits', resourceName, eatAmount + '.png'));
            } else {
              throw 'invalid resource type';
            }
          } else if (type == 'character') {
            return fileUrl(path.join(folderName, id, 'characters', resourceName, 'character.png'));
          } else if (type == 'characterItem') {
            return fileUrl(path.join(folderName, id, 'characters', resourceName, 'item.png'));
          } else if (type == 'characterItemDone') {
            return fileUrl(path.join(folderName, id, 'characters', resourceName, 'item_done.png'));
          } else if (type == 'characterItemUsing') {
            return fileUrl(path.join(folderName, id, 'characters', resourceName, 'item_using.png'));
          } else if (type == 'icon') {
            return fileUrl(path.join(folderName, id, 'icon.png'));
          } else {
            throw 'invalid resource type';
          }
        }
      },
      getMeta: function(location, id, callback) {
        if (location == 'middler') {
          console.error('there are no packages in middler');
          return callback('meta_retrieval_error', undefined);
        }
        var folderName = middler.packages.location.locToFolder(location);
        if (!folderName) {
          console.error('invalid location');
          return callback('meta_retrieval_error', undefined);
        }
        var metaPath = path.join(folderName, id, 'meta.json');
        ipcRenderer.send('read-json', {
          file: metaPath,
          callbackId: middler.callbackMgr.save((data) => {
            if (data.err) {
              console.error('package meta read error:', data.err);
              return callback('meta_retrieval_error', undefined);
            }
            return callback(undefined, data.data);
          })
        });
      },
      getList: function(location, callback) {
        if (location == 'middler') {
          // there are no packages in middler
          return callback(undefined, []);
        }
        var folderName = middler.packages.location.locToFolder(location);
        if (!folderName) {
          console.error('invalid location');
          return callback('meta_retrieval_error', undefined);
        }
        var dir = path.join(folderName + '/');
        ipcRenderer.send('read-dir', {
          dir: dir,
          callbackId: middler.callbackMgr.save((data) => {
            if (data.err) {
              return callback(undefined, []);
            }
            return callback(undefined, data.files);
          })
        });
      },
      install: function(callback) {
        remote.dialog.showOpenDialog(remote.getCurrentWindow(), {
          title: _('packages.manager.install'),
          filters: [
            {
              name: _('packages.fileTypeName'),
              extensions: [ 'gianniapppkg' ]
            }
          ],
          properties: [
            'openFile', 'treatPackageAsDirectory'
          ]
        }).then((data) => {
          if (!data || data.length != 1) {
            return callback('canceled');
          }
          var file = data[0];
          var metaPath = undefined;
          var meta = 'not_read';
          var zipStructure = {};
          var files = [];
          function zipStructureSet(obj, path, value) {
            if (typeof path == 'string') {
              return zipStructureSet(obj, path.split('/'), value);
            } else if (path.length == 1 && obj[path[0]]) {
              return obj[path[0]] = value;
            } else if (path.length != 0) {
              if (!obj[path[0]]) {
                obj[path[0]] = {};
              }
              return zipStructureSet(obj[path[0]], path.slice(1), value);
            }
          }
          function zipStructureFixFile(obj, path, value) {
            if (typeof path == 'string') {
              return zipStructureFixFile(obj, path.split('/'), value);
            } else if (path.length == 1 && obj[path[0]]) {
              return obj[path[0]] = value;
            } else {
              return zipStructureFixFile(obj[path[0]], path.slice(1), value);
            }
          }
          try {
            var zip = new AdmZip(file);
            zipEntries = zip.getEntries();
            for (var entry of zipEntries) {
              var filename = entry.entryName;
              var type = entry.isDirectory ? 'directory' : 'file';
              if (filename.endsWith('/')) {
                filename = filename.substr(0, filename.length - 1);
              }
              if (type == 'file') {
                files.push(filename);
              }
              zipStructureSet(zipStructure, filename, {});
              if (type == 'file' && filename.match(/^[a-z0-9-_]+\/meta\.json$/)) {
                var metaString = zip.readAsText(entry);
                try {
                  meta = JSON.parse(metaString);
                } catch (ex) {
                  meta = 'invalid';
                }
              }
            }
            for (var f of files) {
              zipStructureFixFile(zipStructure, f, 'file');
            }
            // abort if there are files that would be extracted outside the extraction directory
            if (zipStructure['..']) return callback('validation_error');
            var length = 0; // there must be only one folder/file in the compressed file
            var packageId = '';
            for (var i in zipStructure) {
              length++;
              // if zipStructure is valid it is an object with only one element.
              // the package id is the key of that only element.
              // this reads that key
              packageId = i;
            }
            if (length != 1) return callback('validation_error');
            if (packages[packageId] && packages[packageId].location != 'main') {
              return callback('conflict', packageId);
            }
            if (zipStructure[packageId] == 'file') {
              // the zip file contains a single file
              return callback('validation_error');
            }
            if (!zipStructure[packageId]['meta.json']) {
              // does not contain a meta
              return callback('validation_error');
            }
            if (!zipStructure[packageId]['icon.png']) {
              // does not contain a package icon
              return callback('validation_error');
            }
            if (meta == 'invalid') {
              return callback('validation_error');
            }
            if (meta.pkgVersion !== 1) return callback('unsupported');
            if (
              !meta.version
              || !meta.name
              || !meta.description
              || !meta.author
            ) return callback('validation_error');
            if (meta.fruits) {
              for (var fruitId in meta.fruits) {
                if (
                  !zipStructure[packageId].fruits
                  || !zipStructure[packageId].fruits[fruitId]
                  || zipStructure[packageId].fruits[fruitId]['0.png'] !== 'file'
                  || zipStructure[packageId].fruits[fruitId]['1.png'] !== 'file'
                  || zipStructure[packageId].fruits[fruitId]['2.png'] !== 'file'
                  || zipStructure[packageId].fruits[fruitId]['3.png'] !== 'file'
                ) return callback('validation_error');
              }
            }
            if (meta.characters) {
              for (var characterId in meta.characters) {
                if (meta.characters[characterId].item) {
                  if (
                    !zipStructure[packageId].characters
                    || !zipStructure[packageId].characters[characterId]
                    || zipStructure[packageId].characters[characterId]['character.png'] !== 'file'
                    || zipStructure[packageId].characters[characterId]['item.png'] !== 'file'
                    || zipStructure[packageId].characters[characterId]['item_using.png'] !== 'file'
                    || zipStructure[packageId].characters[characterId]['item_done.png'] !== 'file'
                  ) return callback('validation_error');
                } else {
                  if (
                    !zipStructure[packageId].characters
                    || !zipStructure[packageId].characters[characterId]
                    || zipStructure[packageId].characters[characterId]['character.png'] !== 'file'
                  ) return callback('validation_error');
                }
              }
            }
            zip.extractAllTo(middler.packages.location.locToFolder('main'))
            return callback(undefined, packageId);
          } catch (ex) {
            return callback('validation_error');
          }
        });
      },
      uninstall: function(id, location, callback) {
        if (location != 'main') {
          console.error('cannot uninstall packages from a location other than main');
          return callback('cannot_uninstall');
        }
        var folderName = middler.packages.location.locToFolder(location);
        var dir = path.join(folderName, id);
        ipcRenderer.send('delete-dir', {
          dir: dir,
          callbackId: middler.callbackMgr.save((err) => {
            return callback(err);
          })
        });
      }
    },
    getLang: function() {
      if (!window.localStorage.getItem('middlerLang')) {
        var goToLang = knownLangs[0];
        if (typeof navigator.language != 'undefined') {
          var desiredLang = navigator.language.split('-')[0].toLowerCase();
          if (knownLangs.indexOf(desiredLang) > -1) {
            goToLang = desiredLang;
          }
        }
        window.localStorage.setItem('middlerLang', goToLang);
      }
      return window.localStorage.getItem('middlerLang');
    },
    setLang: function(lang) {
      window.localStorage.setItem('middlerLang', lang);
    },
    dialog: function(title, text, button, callback) {
      var cbGen = function(callback) {
        return function(_i) {
          callback();
        }
      }
      middler.dialogMultiple(title, text, [button], cbGen(callback))
    },
    dialogMultiple: function(title, text, btns, callback) {
      var cbGen = function(callback) {
        return function(response) {
          callback(response.response);
        }
      }
      remote.dialog.showMessageBox(remote.getCurrentWindow(), {
        'type': 'none',
        'buttons': btns,
        'title': title,
        'message': text
      }).then(cbGen(callback));
    },
    dialogText: function(title, text, okBtn, cancelBtn, callback) {
      alert('h');
    },
    url: function(url) {
      remote.shell.openExternal(url);
    },
    quit: function() {
      remote.app.quit();
    },
    data: {
      get: function(key) {
        var data = middlerInternal.userData[key];
        if (data) {
          return data;
        } else {
          middlerInternal.userData[key] = {};
          ipcRenderer.send('create-data-file', key);
          return null;
        }
      },
      set: function(key, value) {
        middlerInternal.userData[key] = value;
      },
      delete: function(key) {
        if (key === null) {
          ipcRenderer.send('delete-data', 'all');
        } else {
          ipcRenderer.send('delete-data', key);
        }
      }
    }
  }

  window.dispatchEvent(new Event('framework-ready'));
});

window.addEventListener('game-ready', function() {
  ipcRenderer.send('preload-done');
});

ipcRenderer.send('middler-ok');

ipcRenderer.on('sync-setup', (_e, code) => {
  var dataStr = atob(code);
  var data = JSON.parse(dataStr);
  setupSync(data);
  $('#syncSetupServer')[0].close();
})

ipcRenderer.on('callback', (_e, callbackId, data) => {
  if (typeof middler.callbackMgr.callbacks[callbackId] == 'function') {
    middler.callbackMgr.callbacks[callbackId](data);
    delete middler.callbackMgr.callbacks[callbackId];
  }
})

// migration from previous data storage
window.addEventListener('game-ready', function() {
  var changedSomething = false;
  var fruitConvert = {
    '-2': 'vanilla',
    '-1': 'coin',
    '0': ['gianniapp', 'apple'],
    '1': ['gianniapp', 'pear'],
    '2': ['gianniapp', 'peach'],
    '3': ['gianniapp', 'apricot'],
    '4': ['gianniapp', 'grapes'],
    '5': ['gianniapp', 'watermelon'],
    '6': ['gianniapp', 'banana']
  }
  for (var i = -2; i <= 6; i++) {
    var amount = window.localStorage.getItem('fruit' + i);
    if (amount != null) {
      var convert = fruitConvert[i.toString()];
      if (convert === 'vanilla') {
        var data = getUserData(true);
        data.vanillaFruits = amount;
        setUserData(data, true);
      } else if (convert === 'coin') {
        setCoins(amount);
      } else {
        try {
          setFruit(convert[0], convert[1], amount);
        } catch (ex) {
          console.log('error in migrating fruits (package probably isn\'t loaded):', ex);
        }
      }
      changedSomething = true;
      window.localStorage.removeItem('fruit' + i);
    }
  }
  for (var i = 1; i <= 6; i++) {
    var bought = window.localStorage.getItem('shopBoughtfruits' + i);
    if (bought == 'true') {
      var convert = fruitConvert[i.toString()];
      try {
        var data = getUserData(true);
        if (!data.purchases.fruits[convert[0]]) {
          data.purchases.fruits[convert[0]] = {};
        }
        data.purchases.fruits[convert[0]][convert[1]] = true;
        setUserData(data, true);
      } catch (ex) {
        console.log('error in migrating purchase (package probably isn\'t loaded):', ex);
      }
      changedSomething = true;
      window.localStorage.removeItem('shopBoughtfruits' + i);
    }
  }
  if (changedSomething) {
    refresh();
  }
});
// end of migration