var running = false;
module.exports = () => {
  return new Promise((resolve, reject) => {
    if (running) {
      reject('already running');
    }
    running = true;
    const fs = require("fs-extra");
    fs.access(__dirname + '/config.json').then(() => {
      const config = require('./config.json')
      if (typeof config.game != 'string') {
        console.error('[copier] please specify in config.json the value "game" with a path to the base code');
        running = false;
        reject('copier error, please see log');
      }
      console.log('[copier] emptying game directory');
      fs.emptyDir(__dirname + '/src/game').then(() => {
        console.log('[copier] copying game code');
        fs.copy(config.game, __dirname + '/src/game').then(() => {
          console.log('[copier] copying platform-specific modifications');
          fs.copy(__dirname + '/gianniapp-src', __dirname + '/src/game').then(() => {
            console.log('[copier] done');
            running = false;
            resolve();
          }).catch((err) => {
            console.error('[copier] an error occurred while copying the game code:', err);
            running = false;
            reject('copier error, please see log');
          });
        }).catch((err) => {
          console.error('[copier] an error occurred while copying the game code:', err);
          running = false;
          reject('copier error, please see log');
        });
      }).catch((err) => {
        console.error('[copier] an error occurred while emptying the game directory:', err);
        running = false;
        reject('copier error, please see log');
      });
    }).catch((_err) => {
      console.error('[copier] please create a file called config.json and specify inside it the value "game" with a path to the base code');
      running = false;
      reject('copier error, please see log');
    });
  });
}
