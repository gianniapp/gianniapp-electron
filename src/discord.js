const DiscordRPC = require('discord-rpc');

let discord = {
  clientId: '421739358736089088',
  rpc: null
};

DiscordRPC.register(discord.clientId);
discord.rpc = new DiscordRPC.Client({ transport: 'ipc' });
discord.rpc.login({ clientId: discord.clientId }).catch((err) => {
  console.error('discordrpc login failed', err);
});

module.exports = {
  reloadPresence: async function(mainWindow, startTimestamp) {
    var userData = await mainWindow.webContents.executeJavaScript('window.middlerInternal.userData');
    var packages = await mainWindow.webContents.executeJavaScript('window.packages');
    if (userData.syncable && userData.unsyncable) {
      try {
        var syncable = JSON.parse(userData.syncable);
        var unsyncable = JSON.parse(userData.unsyncable);
      } catch (ex) {
        return console.error('discordrpc: missing some data');
      }
    } else {
      return console.error('discordrpc: missing some data');
    }
    var lang = await mainWindow.webContents.executeJavaScript('window.middler.getLang()');
    var current = await mainWindow.webContents.executeJavaScript('window.current');
    var defaults = await mainWindow.webContents.executeJavaScript('window.defaults');
    var getCoins = function() {
      return syncable.coins;
    }
    var getFruit = function(packageId, fruitId) {
      if (syncable.fruitCounts[packageId]) {
        var amount = syncable.fruitCounts[packageId][fruitId];
        if (!amount) {
          return 0;
        }
        return amount;
      } else if (packages[packageId]) {
        return 0;
      } else {
        throw 'package not loaded';
      }
    }
    var findTranslatedText = function(obj) {
      if (typeof obj == 'string') {
        return obj;
      }
      if (obj[lang]) {
        return obj[lang];
      } else if (obj.default) {
        return obj[obj.default];
      } else if (obj.en) {
        return obj.en;
      } else if (obj.it) {
        return obj.it;
      } else {
        return undefined;
      }
    }
    var _ = async function(str, data) {
      if (typeof str == 'string' && !str.includes('"') && !str.includes('\\')) {
        if (data) {
          return await mainWindow.webContents.executeJavaScript('window._(\"' + str + '\",' + JSON.str(data) + ')');
        } else {
          return await mainWindow.webContents.executeJavaScript('window._(\"' + str + '\")');
        }
      } else {
        throw 'str is not a string or contains invalid characters (" and \\)';
      }
    }
    var currentCharacterId = current.character.id.split('/')
    if (unsyncable.vanilla) {
      discord.rpc.setActivity({
        details: syncable.vanillaFruits + ' ' + findTranslatedText(current.fruit.amount.plural),
        state: await _('settings.vanilla'),
        startTimestamp,
        largeImageKey: 'fruit--' + current.fruit.id.replace('/', '--'),
        largeImageText: findTranslatedText(current.fruit.name),
        smallImageKey: 'special--vanilla',
        smallImageText: await _('settings.vanilla'),
        instance: false
      });
    } else if (current.character.item == null) {
      var currentFruitId = current.fruit.id.split('/');
      discord.rpc.setActivity({
        details: await getFruit(currentFruitId[0], currentFruitId[1]) + ' ' + findTranslatedText(current.fruit.amount.plural),
        state: getCoins() + ' ' + await _('coin.coin_plural'),
        startTimestamp,
        largeImageKey: 'fruit--' + current.fruit.id.replace('/', '--'),
        largeImageText: findTranslatedText(current.fruit.name),
        smallImageKey: 'character--' + current.character.id.replace('/', '--'),
        smallImageText: findTranslatedText(current.character.name),
        instance: false,
      });
    } else {
      var currentFruitId = [];
      for (var i in current.character.item.reward) {
        if (i != 'coin') {
          currentFruitId = i.split('/');
          if (!packages[currentFruitId[0]]) {
            currentFruitId = defaults.fruits.split('/');
          }
          break;
        }
      }
      discord.rpc.setActivity({
        details: getFruit(currentFruitId[0], currentFruitId[1]) + ' ' + findTranslatedText(packages[currentFruitId[0]].fruits[currentFruitId[1]].amount.plural),
        state: getCoins() + ' ' + await _('coin.coin_plural'),
        startTimestamp,
        largeImageKey: 'character--' + current.character.id.replace('/', '--'),
        largeImageText: findTranslatedText(current.character.name),
        smallImageKey: 'item--' + current.character.id.replace('/', '--'),
        smallImageText: findTranslatedText(current.character.item.name),
        instance: false,
      });
    }
  }
}