const { app, ipcMain, nativeTheme, BrowserWindow, Menu } = require('electron');
const { autoUpdater } = require("electron-updater");
const package = require('../package.json');
const fs = require('fs-extra');
const path = require('path');

const discord = require('./discord');

var data = {}, paths = {}, quitWrittenData = false;

autoUpdater.logger = require("electron-log")
autoUpdater.logger.transports.file.level = "info"

var waitingForReady = {};
var isReady = false;

let mainWindow, menu, startTimestamp;
let menuTemplate = [
  {
    label: package.productName,
    submenu: [
      { role: 'quit' }
    ]
  },
  { role: 'editMenu' }
];

async function writeData() {
  var userData = await mainWindow.webContents.executeJavaScript('window.middlerInternal.userData');
  for (var dataName in userData) {
    fs.writeFileSync(path.join(paths.saveTo, dataName + '.json'), userData[dataName], {
      encoding: 'utf8'
    });
  }
}

var createWindow = () => {
  quitWrittenData = false;
  mainWindow = new BrowserWindow({
    'width': 800,
    'height': 600,
    'webPreferences': {
      'nodeIntegration': false,
      'preload': __dirname + '/preload.js',
      'contextIsolation': false
    }
  });
  mainWindow.on('closed', () => {
    if (process.platform !== 'darwin') {
      app.quit();
    }
    mainWindow = null;
  });
  mainWindow.on('close', function(e) {
    if (!quitWrittenData) {
      e.preventDefault();
      writeData().then(() => {
        quitWrittenData = true;
        mainWindow.close();
      });
    }
  });
  menu = Menu.buildFromTemplate(menuTemplate);
  Menu.setApplicationMenu(menu);
  //mainWindow.webContents.openDevTools();
  mainWindow.loadFile('src/game/index.html');
}

function handleURLOpen(url) {
  if (url.startsWith('gianniapp-electron://sync-setup/?') || url.startsWith('gianniapp-electron://sync-setup?')) {
    // gianniSync setup return URL
    var query = url.split('?')[1];
    query = query.split('&');
    var code = null;
    for (var queryPartStr of query) {
      var queryPart = queryPartStr.split('=');
      if (decodeURIComponent(queryPart[0]).toLowerCase() == 'giannisyncsetupcode') {
        code = decodeURIComponent(queryPart[1]);
      }
    }
    if (code != null) {
      if (isReady) mainWindow.webContents.send('sync-setup', code);
      else waitingForReady['sync-setup'] = code;
    }
  }
}

function processCommandLine(commandLine) {
  if (commandLine.length == 2) {
    if (commandLine[1].startsWith('gianniapp-electron://')) {
      handleURLOpen(commandLine[1]);
    }
  }
}

app.on('open-url', function(e, url) {
  e.preventDefault();
  handleURLOpen(url);
});

if (!app.requestSingleInstanceLock()) {
  app.quit();
} else {
  app.on('second-instance', (event, commandLine, workingDirectory) => {
    if (mainWindow === null) {
      createWindow()
    } else {
      processCommandLine(commandLine);
      if (mainWindow.isMinimized()) mainWindow.restore()
      mainWindow.focus()
    }
  });

  app.setAsDefaultProtocolClient('gianniapp-electron');

  app.on('ready', () => {
    paths = {
      temp: app.getPath('temp'),
      data: path.dirname(app.getPath('exe')),
      managedPackages: path.join(path.dirname(app.getPath('exe')), 'managed-packages')
    };
    if (process.platform == 'darwin') {
      // if macOS then the path should be the one to the directory in which the .app directory is
      // except if the app is in the Applications directory
      paths.data = path.dirname(paths.data.substr(0, paths.data.lastIndexOf('.app') + 4));
      paths.managedPackages = path.join(paths.data, 'managed-packages')
      if (paths.data.endsWith('/Applications')) {
        paths.data = app.getPath('userData');
      }
    }
    fs.access(paths.data, fs.constants.W_OK || fs.constants.R_OK, (err) => {
      if (err) paths.data = app.getPath('userData');
      paths.packages = path.join(paths.data, 'packages');
      paths.saveTo = path.join(paths.data, 'data');
      createWindow();
    });
    autoUpdater.setFeedURL({
      'provider': 'generic',
      'url': 'https://gianniapp.altervista.org/res/files/gianniapp-electron',
      'channel': 'latest'
    });
    autoUpdater.checkForUpdatesAndNotify();
  });
  
  app.on('window-all-closed', () => {
    app.quit();
  });
  
  app.on('activate', () => {
    if (mainWindow === null) {
      createWindow()
    }
  });
  
  ipcMain.on('middler-ok', () => {
    fs.ensureDirSync(paths.packages);
    fs.ensureDirSync(paths.saveTo);
    var dataFiles = fs.readdirSync(paths.saveTo);
    for (var filename of dataFiles) {
      var str = fs.readFileSync(path.join(paths.saveTo, filename)).toString('utf8');
                 /* substr to remove '.json' from the end of the filename */
      data[filename.substr(0, filename.length - 5)] = str;
    }
  
    setInterval(() => {
      writeData();
    }, 10e3);
  
    mainWindow.webContents.send('main-ok', {
      paths: paths,
      userData: data,
      dark: (process.platform == 'darwin') ? nativeTheme.shouldUseDarkColors : null
    });
  });
  
  ipcMain.on('preload-done', () => {
    isReady = true;
    startTimestamp = new Date();
    discord.reloadPresence(mainWindow, startTimestamp);
    setInterval(() => {
      discord.reloadPresence(mainWindow, startTimestamp);
    }, 15e3);
    if (waitingForReady['sync-setup']) mainWindow.webContents.send('sync-setup', waitingForReady['sync-setup']);
  });
  
  ipcMain.on('read-json', (_e, data) => {
    fs.readJSON(data.file).then((fileData) => {
      mainWindow.webContents.send('callback', data.callbackId, {
        data: fileData,
        err: undefined
      });
    }).catch((err) => {
      mainWindow.webContents.send('callback', data.callbackId, {
        data: undefined,
        err: err
      });
    });
  });
  
  ipcMain.on('read-dir', (_e, data) => {
    fs.readdir(data.dir).then((files) => {
      mainWindow.webContents.send('callback', data.callbackId, {
        files: files,
        err: undefined
      });
    }).catch((err) => {
      mainWindow.webContents.send('callback', data.callbackId, {
        files: undefined,
        err: err
      });
    });
  });
  
  ipcMain.on('delete-dir', (_e, data) => {
    fs.remove(data.dir).then(() => {
      mainWindow.webContents.send('callback', data.callbackId, undefined);
    }).catch((err) => {
      mainWindow.webContents.send('callback', data.callbackId, err);
    });
  });
  
  ipcMain.on('create-data-file', (_e, data) => {
    fs.writeFileSync(path.join(paths.saveTo, data + '.json'), '{}', {
      encoding: 'utf8'
    });
  });
  
  ipcMain.on('delete-data', (_e, data) => {
    if (data == 'all') {
      quitWrittenData = true;
      fs.emptyDirSync(path.join(paths.saveTo));
      setTimeout(function() { quitWrittenData = false; }, 50);
    } else {
      fs.unlinkFileSync(path.join(paths.saveTo, data + '.json'));
    }
  });

  processCommandLine(process.argv);
}